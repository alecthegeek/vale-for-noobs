# Introduction

Vale is an awesome tool, but when you fist use you have to read a lot of documentation and the output can appear overwelaming.

This document is a quick start to bootstrap new users who want to get Vale command line tool working in an existing project
so they can learn more.

This document does not replace the official Vale [docs](https://vale.sh/docs/).You should read those at the same time.

# Installing Vale

* Follow the [instructions](https://vale.sh/docs/vale-cli/installation/) on the website
  * Note: You can install Vale into an existing container image by adding the line

   ```
   COPY --from=jdkato/vale:latest /bin/vale /usr/local/bin
   ```
  
  in your Dockerfile. Depedning on the markup language in use other depedendencies may be needed.

# Adding Vale to your existing project

* Follow the [instructions](https://vale.sh/docs/vale-cli/structure/) on the website to set up Vale and the `.vale.ini` config file
* New Vale users should start off with a `MinAlertLevel` of `error` to avoid being overwehlweld with messages. Once those are fixed then try `warning` once you feel more prepared
* To get started use the exisiting prepackaged rules by setting `Packages` to `write-good, Readability`. You can also add a third party package such as `Google` or `Microsoft`
* Don't forget to run `vale sync` when you change the Package list
    
# Scoping

#TODO

* Test regex patterns in sites like [Regex101](https://regex101.com/)

#TODO  It's possible that users will need to use scoping rules (e.g. to manage URL links in RST) even before they write rules

# Writing your own rules

* Introduce one rule at a time when using Vale in a CI/CD pipeline
  (taken from  https://docs.gitlab.com/ee/development/documentation/testing.html#when-to-add-a-new-vale-rule)
* Use Vale [Studio](https://studio.vale.sh/) to create and test new rules
* Check if rules already exist in other repositories or in the Vale [Rule Explorer](https://vale.sh/explorer/)
* Don't add rules mindlessly — let existing style guides inform your choices
* Consider checking only added or updated content in CI/CD pipelines
* Use the official Docker image in CI/CD
* When facing obstacles, ask in #testthedocs, then raise an issue
* Consider packaging your style

# Other Resources

#TODO
